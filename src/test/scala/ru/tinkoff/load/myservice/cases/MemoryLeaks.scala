package ru.tinkoff.load.myservice.cases

import io.gatling.core.Predef._
import io.gatling.http.Predef._

object MemoryLeaks {

  val ml1 = http("ml1 /get")
    .get("/memoryleaks/1")
    .queryParam("counter", "${seqInt}")
    .check(status is 200)
    .check(regex("""Memory Leak Through static Fields - (\w+)""").is("counter"))

  val ml2 = http("m2 /get")
    .get("/memoryleaks/2")
    .queryParam("counter", "${seqInt}")
    .check(status is 200)
    .check(regex("""Improper equals() and hashCode() Implementations - (\w+)""").is("counter"))

  val ml3 = http("m3 /get")
    .get("/memoryleaks/3")
    .queryParam("url", "${URL}")
    .check(status is 200)
    .check(regex("""UnclosedStream""").exists)
}
