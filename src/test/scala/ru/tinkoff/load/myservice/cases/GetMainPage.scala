package ru.tinkoff.load.myservice.cases

import io.gatling.http.Predef._
import io.gatling.core.Predef._

object GetMainPage {

  val getMainPage = http("GET /get")
    .get("/get")
    .queryParam("session", "${randomStr}")
    .check(status is 200)
    .check(regex("""Successful (\w+) response""").is("get"))

  val postMainPage = http("POST /post")
    .post("/post")
    .queryParam("session", "${randomStr}")
    .check(status.not(404))
    .check(regex("""Successful (\w+) response""").is("post"))
}
