package ru.tinkoff.load.myservice.scenarios

import io.gatling.core.Predef._
import io.gatling.core.structure.ScenarioBuilder
import ru.tinkoff.load.myservice.cases._
import ru.tinkoff.load.myservice.feeders.Feeders._

object CommonScenario {
  def apply(): ScenarioBuilder = new CommonScenario().scn
}

class CommonScenario {

  val scn: ScenarioBuilder = scenario("Common Scenario")
    .feed(randomString)
    .feed(seqInt)
    .feed(resURL)
    .randomSwitch(
      25.0 -> exec(GetMainPage.getMainPage),
      25.0 -> exec(GetMainPage.postMainPage),
      20.0 -> exec(MemoryLeaks.ml1),
      20.0 -> exec(MemoryLeaks.ml2),
      10.0 -> exec(MemoryLeaks.ml3)
    )

}