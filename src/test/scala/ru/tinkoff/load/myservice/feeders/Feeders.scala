package ru.tinkoff.load.myservice.feeders

import ru.tinkoff.gatling.feeders._
import io.gatling.core.Predef._

object Feeders {
  val randomString = RandomRangeStringFeeder("randomStr", 10, 15, "qwerty12345")

  val seqInt = SequentialFeeder("seqInt", 10000, 1)

  val resURL = csv("pools/resURL.csv").random
}
